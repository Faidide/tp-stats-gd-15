all: rapport.pdf

rapport.tex: rapport.rnw
	R -e 'library(knitr);knit("rapport.rnw")'

rapport.pdf: rapport.tex
	pdflatex rapport.tex

clean:
	rm -rf cache
	rm rapport.tex rapport.pdf rapport.log rapport.aux rapport.fdb_latexmk rapport.fls rapport-concordance.tex rapport.toc